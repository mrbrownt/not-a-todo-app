import React, { Suspense, lazy } from "react"
import { BrowserRouter, Switch, Route } from "react-router-dom"

const Home = lazy(() => import("./pages/Home"))
const Login = lazy(() => import("./pages/Login"))
const NotATodo = lazy(() => import("./pages/NotATodo"))

export default function Router() {
  return (
    <BrowserRouter>
      <Suspense fallback={<div>Loading</div>}>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route path="/app">
            <NotATodo />
          </Route>
        </Switch>
      </Suspense>
    </BrowserRouter>
  )
}
