import React from "react"
import {
  Container,
  Typography,
  makeStyles,
  createStyles,
  Theme,
  useTheme,
} from "@material-ui/core"
import HomeAppBar from "./HomeAppBar"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    centerContent: {
      marginTop: theme.spacing(8),
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
  }),
)

export default function Home() {
  const theme = useTheme()
  const classes = useStyles(theme)

  return (
    <>
      <HomeAppBar />
      <Container>
        <Typography className={classes.centerContent} variant="h3">
          This Is Not a Todo App
        </Typography>
      </Container>
    </>
  )
}
