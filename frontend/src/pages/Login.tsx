import React from "react"
import {
  Container,
  Card,
  Typography,
  CardContent,
  makeStyles,
  createStyles,
  useTheme,
  Grid,
  Button,
  Link,
} from "@material-ui/core"
import AssignmentTurnedInIcon from "@material-ui/icons/AssignmentTurnedIn"
import GithubIcon from "@material-ui/icons/GitHub"
import useLinkColor from "../hooks/useLinkColor"
import { useHistory } from "react-router-dom"

const useStyles = makeStyles((theme) =>
  createStyles({
    root: { marginTop: theme.spacing(8) },
  }),
)

export default function Login() {
  const theme = useTheme()
  const classes = useStyles(theme)
  const linkColor = useLinkColor()
  const history = useHistory()

  function fakeLogin() {
    history.push("/app")
  }

  return (
    <Container className={classes.root} component="main" maxWidth="sm">
      <Card variant="outlined">
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography variant="h6">
                <AssignmentTurnedInIcon fontSize="small" />
                Not A Todo App
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h4">Sign Up</Typography>
            </Grid>
            <Grid item xs={12}>
              <Button
                fullWidth
                size="large"
                variant="outlined"
                onClick={fakeLogin}
              >
                Continue with GitLab
              </Button>
            </Grid>
            <Grid item xs={12}>
              <Button
                startIcon={<GithubIcon />}
                fullWidth
                size="large"
                variant="outlined"
                onClick={fakeLogin}
              >
                Continue with GitHub
              </Button>
            </Grid>
            <Grid item xs={12}>
              <Button
                fullWidth
                size="large"
                variant="outlined"
                onClick={fakeLogin}
              >
                Continue with Atlassian
              </Button>
            </Grid>
            <Grid item xs={12}>
              By continuing I now own your soul! Read the{" "}
              <Link color={linkColor} href="/eula">
                EULA
              </Link>{" "}
              here.
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Container>
  )
}
