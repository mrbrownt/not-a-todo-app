import React from "react"
import {
  AppBar,
  Typography,
  makeStyles,
  createStyles,
  Theme,
  Toolbar,
  useTheme,
} from "@material-ui/core"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
    },
    toolbar: {
      "& > *": {
        margin: theme.spacing(1),
      },
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
  }),
)

export default function HomeAppBar() {
  const theme = useTheme()
  const classes = useStyles(theme)
  // const history = useHistory()

  return (
    <AppBar className={classes.appBar} position="fixed" elevation={0}>
      <Toolbar className={classes.toolbar}>
        <Typography className={classes.grow} variant="h6">
          NotATodoApp
        </Typography>
      </Toolbar>
    </AppBar>
  )
}
