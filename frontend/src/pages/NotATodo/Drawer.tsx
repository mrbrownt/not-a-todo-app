import React from "react"
import {
  Drawer,
  makeStyles,
  createStyles,
  useTheme,
  Toolbar,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Typography,
  Tooltip,
} from "@material-ui/core"
import MergeTypeIcon from "@material-ui/icons/MergeType"
import LaunchIcon from "@material-ui/icons/Launch"
import ExpandMore from "@material-ui/icons/ExpandMore"
import { useHistory } from "react-router-dom"

const drawerWidth = 320

const useStyles = makeStyles((theme) =>
  createStyles({
    drawer: {
      width: drawerWidth,
      // flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerContainer: {
      overflow: "auto",
    },
  }),
)

export default function AppDrawer() {
  const theme = useTheme()
  const classes = useStyles(theme)
  const history = useHistory()

  function openMerge() {
    history.push("/app/someID")
  }

  return (
    <Drawer
      className={classes.drawer}
      classes={{ paper: classes.drawerPaper }}
      variant="permanent"
    >
      <div className={classes.drawerContainer}>
        <Toolbar />
        <List>
          <ListItem button selected onClick={openMerge}>
            <ListItemIcon>
              <MergeTypeIcon />
            </ListItemIcon>
            <ListItemText
              primary={
                <Tooltip title="WIP: Add Carrier Configuration Database">
                  <Typography noWrap>
                    WIP: Add Carrier Configuration Database
                  </Typography>
                </Tooltip>
              }
            />
            <ListItemIcon>
              <LaunchIcon />
            </ListItemIcon>
            <ExpandMore />
          </ListItem>
        </List>
      </div>
    </Drawer>
  )
}
