import React from "react"
import AppBar from "./AppBar"
import AppDrawer from "./Drawer"
import { makeStyles, createStyles } from "@material-ui/core"
import MergeRequest from "./MergeRequest"

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: "flex",
    },
  }),
)

export default function NotATodo() {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <AppBar />
      <AppDrawer />
      <MergeRequest />
    </div>
  )
}
