import React from "react"
import {
  Card,
  CardHeader,
  Container,
  Toolbar,
  CardContent,
  Typography,
  makeStyles,
  createStyles,
  Grid,
  TextField,
  Avatar,
  Button,
} from "@material-ui/core"
import useLinkColor from "../../hooks/useLinkColor"

const useStyles = makeStyles((theme) =>
  createStyles({
    card: {
      marginTop: theme.spacing(2),
    },
  }),
)

export default function MergeRequest() {
  const classes = useStyles()

  return (
    <Container>
      <Toolbar />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Card className={classes.card}>
            <CardHeader title="WIP: Add Carrier Configuration Database" />
            <CardContent>
              <Typography variant="h5"></Typography>
              <Typography variant="h6"> Renovate configuration</Typography>
              <Typography>
                <br /> 📅 Schedule: At any time (no schedule defined).
                <br /> 🚦 Automerge: Disabled by config. Please merge this
                manually once you are satisfied.
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12}>
          <Card>
            <CardHeader title="Comments" />
            <CardContent>
              <Grid container spacing={2} direction="column" xs={12}>
                <Grid item>
                  <Grid container spacing={1} direction="row">
                    <Grid item>
                      <Avatar
                        alt="Todd Brown"
                        src="https://secure.gravatar.com/avatar/b28e6f45e7b33fb5fdd3ea1cd577c283?s=800&d=identicon"
                      />
                    </Grid>
                    <Grid item>
                      <Typography>Todd Brown</Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Typography>Stuffs</Typography>
                </Grid>
                <Grid item>
                  <TextField
                    label="Write"
                    multiline
                    rows={4}
                    color="secondary"
                    variant="outlined"
                    fullWidth
                  />
                  <Grid container direction="row">
                    <Button>Submit</Button>
                    <Button>Cancel</Button>
                  </Grid>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  )
}
