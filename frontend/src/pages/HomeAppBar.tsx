import React from "react"
import {
  AppBar,
  Typography,
  makeStyles,
  createStyles,
  Theme,
  Button,
  Toolbar,
} from "@material-ui/core"
import { useHistory } from "react-router-dom"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
    },
    toolbar: {
      "& > *": {
        margin: theme.spacing(1),
      },
    },
  }),
)

export default function HomeAppBar() {
  const classes = useStyles()
  const history = useHistory()

  function navigateToLogin() {
    history.push("/login")
  }

  function navigateToSignup() {
    history.push("/login")
  }

  return (
    <AppBar position="static" elevation={0}>
      <Toolbar className={classes.toolbar}>
        <Typography className={classes.grow} variant="h6">
          NotATodoApp
        </Typography>
        <Button color="inherit" onClick={navigateToLogin}>
          Login
        </Button>
        <Button
          color="secondary"
          variant="contained"
          onClick={navigateToSignup}
        >
          Signup
        </Button>
      </Toolbar>
    </AppBar>
  )
}
