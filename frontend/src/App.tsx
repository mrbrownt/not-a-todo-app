import React from "react"
import Router from "./Router"
import {
  ThemeProvider,
  CssBaseline,
  createMuiTheme,
  useMediaQuery,
} from "@material-ui/core"
import { red } from "@material-ui/core/colors"

function App() {
  const perfersDarkMode = useMediaQuery("(prefers-color-scheme: dark)")

  const theme = createMuiTheme({
    palette: {
      type: perfersDarkMode ? "dark" : "light",
      primary: {
        main: "#880e4f",
      },
      secondary: {
        main: "#80deea",
      },
      error: {
        main: red.A400,
      },
    },
  })

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router></Router>
    </ThemeProvider>
  )
}

export default App
