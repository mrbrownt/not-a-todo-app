import { useTheme, LinkProps } from "@material-ui/core"

export default function useLinkColor(): LinkProps["color"] {
  const theme = useTheme()

  if (theme.palette.type === "light") {
    return "primary"
  }

  return "secondary"
}
